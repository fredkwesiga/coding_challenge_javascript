function Auction(initialPrice, bids) {

  let maxPrice = bids[0]['bidPrice']

  for(let i = 0; i<bids.length; i++ ) {

    if(bids[i]['bidPrice'] >= initialPrice && bids[i]['bidPrice'] > maxPrice ) {
      maxPrice = bids[i]['bidPrice']
    }
  }

  let getMaximumBid = bids.filter((value)=>{
       if(value.bidPrice === maxPrice) {
          return {name:value.name, bidPrice:value.bidPrice}
       }      
       return false
  })

  return `Highest Bidder: ${getMaximumBid[0]['name']} Maximum price of bid: ${getMaximumBid[0]['bidPrice']}`
}
//Running output to the locally these are for commenting and testinggit
console.log(Auction(1, [{name:'A', bidPrice:5}, {name:'B', bidPrice:10}, {name:'A', bidPrice:8}, {name:'A', bidPrice:27}, {name:'B', bidPrice:17}]))
console.log(Auction (1, [{name:'Henry', bidPrice:15}, {name:'Mukasa', bidPrice:24}, {name:'Barbra', bidPrice:30}, {name:'Asiimwe', bidPrice:31}, {name:'Mark', bidPrice:74}, {name: 'Tim', bidPrice:57}, {name: 'Frank', bidPrice:78}, {name: 'Hellen', bidPrice:61}, {name: 'Atim', bidPrice:64}, {name: 'Mukasa', bidPrice:74}, {name: 'Mukasa', bidPrice:69}, {name: 'Mukasa', bidPrice:71}, {name: 'Hellen', bidPrice:78}, {name: 'Frank', bidPrice:75}, {name: 'Barbra', bidPrice:95}, {name: 'Frank', bidPrice:103}, {name: 'Asiimwe', bidPrice:135}]))
console.log(Auction (1, [{name:'Mathius', bidPrice: 5}, {name: 'Ruth', bidPrice: 10}, {name:'Carl', bidPrice: 19}, {name: 'Mathius', bidPrice:14}, {name: 'Mathias', bidPrice:23}, {name:'Carl', bidPrice:24}, {name:'Carl', bidPrice:25}, {name: 'Ruth', bidPrice:26}]))
console.log(Auction(1, [{ name: 'Lutaya', bidPrice:5}, {name:'Otim', bidPrice:10}, {name: 'Sheila', bidPrice:19}, {name: 'Lutaya', bidPrice:23}, {name: 'Sheila', bidPrice:24}, {name: 'Lutaya', bidPrice:29}, {name:'Otim', bidPrice: 26} ]))
