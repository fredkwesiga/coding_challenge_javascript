

function findOdd(A, arr_size) {
  //happy coding!
  for (let i = 0; i < arr_size; i++) {
    let count = 0;

    for (let j = 0; j < arr_size; j++) {
      if (A[i] == A[j]) {
        count++;
      }
    }
    if (count % 2 != 0) {
      return A[i];
    }
  }

  return 0;
}
//Testing the solution 
let A = [20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5];
let n = A.length;

console.log(findOdd(A, n));


function findOdd(B, arr_size) {
  //happy coding!
  for (let i = 0; i < arr_size; i++) {
    let count = 0;

    for (let j = 0; j < arr_size; j++) {
      if (B[i] == B[j]) {
        count++;
      }
    }
    if (count % 2 != 0) {
      return B[i];
    }
  }

  return 0;
}
//Testing the solution  
let B = [1, 1, 2, -2, 5, 2, 4, 4, -1, -2, 5];
let m = B.length;

console.log(findOdd(B, m));


function findOdd(C, arr_size) {
  //happy coding!
  for (let i = 0; i < arr_size; i++) {
    let count = 0;

    for (let j = 0; j < arr_size; j++) {
      if (C[i] == C[j]) {
        count++;
      }
    }
    if (count % 2 != 0) {
      return C[i];
    }
  }

  return 0;
}
//Testing the solution 
let C = [1, 1, 1, 1, 1, 1, 10, 1, 1, 1, 1];
let r = C.length;

console.log(findOdd(C, r));
